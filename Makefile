
# Format the repository with Treefmt
fmt:
	treefmt .

# Run all the checks of flake
check:
	nix flake check .

# Build the package with useful logs
target := default
build:
	nix build --json --no-link --print-build-logs ".#${target}"
