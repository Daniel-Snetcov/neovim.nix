{
  projectRootFile = "flake.nix";
  flakeCheck = true;
  flakeFormatter = true;
  programs = {
    deadnix.enable = true;
    statix.enable = true;
    alejandra.enable = true;
    stylua.enable = true;
  };
  settings.formatter = {
    stylua.options = [
      "--config-path"
      (builtins.toString (./. + "/stylua.toml"))
    ];
  };
  settings.global.excludes = [
    "./result/**"
  ];
}
