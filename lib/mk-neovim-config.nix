{pkgs, ...}: let
  neovimConfig = pkgs.vimUtils.buildVimPlugin {
    name = "neovimConfig";
    src = ../config;
    postInstall = ''
      rm -rf $out/**/*.nix
    '';
  };
  entries = import ../config/lua/plugins {inherit pkgs;};
in {
  plugins =
    [
      neovimConfig
    ]
    ++ entries.plugins;
  inherit (entries) extraPackages;
}
