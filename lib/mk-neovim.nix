{pkgs, ...}: let
  config = import ./mk-neovim-config.nix {inherit pkgs;};
  customRC = ''
    lua << EOF
        require('core').init()
    EOF
  '';
  extraMakeWrapperArgs = ''
    --suffix PATH : "${pkgs.lib.makeBinPath config.extraPackages}"
  '';
in
  pkgs.neovim.override {
    inherit extraMakeWrapperArgs;
    configure = {
      inherit customRC;
      packages.main = {
        start = config.plugins;
      };
    };
  }
