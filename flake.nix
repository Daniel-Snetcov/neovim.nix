{
  description = "Yet another Neovim flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    treefmt-nix.url = "github:numtide/treefmt-nix";
    pre-commit-hooks-nix.url = "github:cachix/pre-commit-hooks.nix";
  };

  outputs = inputs @ {
    flake-parts,
    self,
    ...
  }:
    flake-parts.lib.mkFlake {inherit inputs;} {
      imports = with inputs; [
        treefmt-nix.flakeModule
        pre-commit-hooks-nix.flakeModule
      ];
      systems = ["x86_64-linux" "aarch64-linux" "aarch64-darwin" "x86_64-darwin"];
      perSystem = {
        config,
        pkgs,
        system,
        ...
      }: {
        _module.args.pkgs = import inputs.nixpkgs {
          config.allowUnfree = true;
          inherit system;
        };
        packages.default = self.lib.mkNeovim {inherit pkgs;};
        devShells.default = pkgs.mkShell {
          inputsFrom = [
            config.treefmt.build.devShell
            config.pre-commit.devShell
          ];
          nativeBuildInputs = [];
        };
        treefmt = import ./treefmt.nix;
        pre-commit = import ./pre-commit.nix {inherit config;};
      };
      flake = {
        lib = import ./lib;
      };
    };
}
