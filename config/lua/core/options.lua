local M = {}

M.opts = {
  fileencoding = 'utf-8',
  mouse = 'a',
  splitbelow = true,
  splitright = true,
  swapfile = false,
  number = true,
  relativenumber = true,
  signcolumn = 'yes',
  colorcolumn = '80',
  autoindent = true,
  expandtab = true,
  tabstop = 2,
  shiftwidth = 2,
  showmode = false,
  termguicolors = true,
}

M.init = function()
  for opt, value in pairs(M.opts) do
    vim.opt[opt] = value
  end
end

return M
