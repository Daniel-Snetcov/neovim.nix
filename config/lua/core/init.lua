local M = {}

M.init = function()
  require('core.options').init()
  require('core.keymaps').init()
  require('plugins').init()
  require('misc').init()
end

return M
