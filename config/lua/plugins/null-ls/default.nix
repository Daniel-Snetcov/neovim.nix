{pkgs, ...}: {
  extraPackages = with pkgs; [
    # Nix
    statix
    deadnix
    alejandra

    eslint_d

    hadolint

    # misc
    taplo

    terraform
    tfsec
  ];
  plugins = with pkgs.vimPlugins; [
    null-ls-nvim
    luasnip
  ];
}
