local M = {}
local null_ls = require('null-ls')
local formatting = null_ls.builtins.formatting
local diagnostics = null_ls.builtins.diagnostics
local completion = null_ls.builtins.completion
local code_actions = null_ls.builtins.code_actions

M.opts = {
  sources = {
    -- nix
    formatting.alejandra,
    diagnostics.deadnix,
    diagnostics.statix,
    code_actions.statix,

    formatting.stylua,
    completion.luasnip,

    diagnostics.hadolint,

    -- webdev
    formatting.eslint_d,
    diagnostics.eslint_d,
    code_actions.eslint_d,

    -- misc
    formatting.taplo,

    -- terraform
    diagnostics.terraform_validate,
    diagnostics.tfsec,
    formatting.terraform_fmt,
  },
  on_attach = function(client, buffer)
    local opts = { buffer = buffer, remap = false }
    vim.keymap.set('n', '<leader>fm', function()
      vim.lsp.buf.format({
        bufnr = buffer,
        -- Never request typescript-language-server for formatting
        filter = function(client)
          return client.name ~= 'tsserver'
        end,
      })
    end, opts)
  end,
}

M.init = function()
  null_ls.setup(M.opts)
end

return M
