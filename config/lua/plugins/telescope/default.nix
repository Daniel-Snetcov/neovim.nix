{pkgs, ...}: {
  extraPackages = with pkgs; [
    # Suggested dependencies
    fzf
    ripgrep

    # Optional dependencies
    fd
  ];
  plugins = with pkgs.vimPlugins; [
    # Required dependencies
    plenary-nvim
    telescope-nvim
    # Improved sorting performance
    telescope-fzf-native-nvim
  ];
}
