local M = {}

M.init = function()
  require('plugins.telescope.telescope').init()
end

return M
