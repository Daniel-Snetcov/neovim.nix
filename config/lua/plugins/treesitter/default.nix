{pkgs, ...}: {
  extraPackages = [];
  plugins = with pkgs.vimPlugins; [
    nvim-treesitter.withAllGrammars
    nvim-treesitter-context
  ];
}
