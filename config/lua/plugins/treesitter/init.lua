local M = {}

M.init = function()
  require('plugins.treesitter.treesitter').init()
  require('plugins.treesitter.treesitter-context').init()
end

return M
