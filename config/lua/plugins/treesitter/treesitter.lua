local M = {}
M.opts = {
  ensure_installed = {},
  sync_install = false,
  auto_install = false,
  ignore_install = {},

  highlight = {
    highlight = { enable = true },
    indent = { enable = true },
    rainbow = { enable = true },
  },
}
M.init = function()
  require('nvim-treesitter.configs').setup(M.opts)
end
return M
