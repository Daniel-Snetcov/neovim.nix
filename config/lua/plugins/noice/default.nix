{pkgs, ...}: {
  extraPackages = [];
  plugins = with pkgs.vimPlugins; [
    noice-nvim

    nui-nvim
    nvim-notify
    nvim-treesitter
  ];
}
