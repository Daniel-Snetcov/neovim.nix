local M = {}
local neodev = require('neodev')

M.opts = {}
M.init = function()
  neodev.setup(M.opts)
end

return M
