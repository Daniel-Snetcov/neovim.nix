local M = {}
local ncomment = require('Comment')

M.opts = {}
M.init = function()
  ncomment.setup(M.opts)
end

return M
