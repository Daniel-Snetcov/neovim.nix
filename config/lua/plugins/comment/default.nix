{pkgs, ...}: {
  extraPackages = [];
  plugins = with pkgs.vimPlugins; [
    comment-nvim
  ];
}
