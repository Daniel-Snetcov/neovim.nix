{pkgs, ...}: {
  extraPackages = [];
  plugins = with pkgs.vimPlugins; [
    nvim-web-devicons
  ];
}
