local M = {}
local devicons = require('nvim-web-devicons')

M.opts = {}
M.init = function()
  devicons.setup(M.opts)
end

return M
