{pkgs, ...}: let
  inherit (builtins) foldl' map filter attrNames readDir;
  isDir = content: dirContent."${content}" == "directory";
  toEntry = name: (import (./. + "/${name}")) {inherit pkgs;};
  dirContent = readDir ./.;
  dirContentNames = attrNames dirContent;
  entries = map toEntry (filter isDir dirContentNames);

  accPlugins = acc: entry: acc ++ entry.plugins;
  accExtraPackages = acc: entry: acc ++ entry.extraPackages;
in {
  plugins = foldl' accPlugins [] entries;
  extraPackages = foldl' accExtraPackages [] entries;
}
