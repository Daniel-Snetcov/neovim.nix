{pkgs, ...}: {
  extraPackages = [];
  plugins = with pkgs.vimPlugins; [
    catppuccin-nvim
  ];
}
