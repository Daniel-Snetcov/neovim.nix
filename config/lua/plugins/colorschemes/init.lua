local M = {}

M.init = function()
  require('plugins.colorschemes.catppuccin').init()
end

return M
