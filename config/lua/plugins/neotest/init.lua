local M = {}
local neotest = require('neotest')
local neotest_jest = require('plugins.neotest.neotest-jest')

M.opts = {
  adapters = {
    neotest_jest.get_adapter(),
  },
}
local function setup_keymaps()
  local opts = { remap = false }
  vim.keymap.set('n', '<leader>mr', neotest.run.run, opts)
  vim.keymap.set('n', '<leader>ms', neotest.run.stop, opts)
  vim.keymap.set('n', '<leader>mo', neotest.output.open, opts)
  vim.keymap.set('n', '<leader>mO', function()
    neotest.output.open({ enter = true })
  end, opts)
  vim.keymap.set('n', '<leader>mS', neotest.summary.toggle, opts)
  vim.keymap.set('n', '<leader>mf', function()
    neotest.run.run(vim.fn.expand('%'))
  end, opts)
  vim.keymap.set('n', '<leader>mn', function()
    neotest.jump.next({ status = 'failed' })
  end, opts)
  vim.keymap.set('n', '<leader>mp', function()
    neotest.jump.prev({ status = 'failed' })
  end, opts)
end

M.init = function()
  neotest.setup(M.opts)
  setup_keymaps()
end

return M
