local M = {}
local neotest_jest = require('neotest-jest')

M.opts = {
  jestCommand = 'npm test --',
  jestConfigFile = 'custom.jest.config.ts',
  env = { CI = true },
  cwd = function(path)
    return vim.fn.getcwd()
  end,
}
M.get_adapter = function()
  return neotest_jest(M.opts)
end

return M
