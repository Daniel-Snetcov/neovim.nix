{pkgs, ...}: {
  extraPackages = [
  ];
  plugins = with pkgs.vimPlugins; [
    neotest
    neotest-jest

    # deps
    plenary-nvim
    FixCursorHold-nvim
    nvim-treesitter
  ];
}
