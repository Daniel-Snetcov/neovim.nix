local M = {}

M.init = function()
  require('plugins.neodev').init()
  require('plugins.lspconfig').init()
  require('plugins.null-ls').init()
  require('plugins.cmp').init()
  require('plugins.treesitter').init()
  require('plugins.telescope').init()
  require('plugins.colorschemes').init()
  require('plugins.harpoon').init()
  require('plugins.gitsigns').init()
  require('plugins.web-devicons').init()
  require('plugins.lualine').init()
  require('plugins.noice').init()
  require('plugins.autopairs').init()
  require('plugins.comment').init()
  require('plugins.neotest').init()
  require('plugins.oil').init()
end

return M
