local M = {}
local gitsigns = require('gitsigns')

M.opts = {}
M.init = function()
  gitsigns.setup(M.opts)
end

return M
