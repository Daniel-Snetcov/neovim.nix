{pkgs, ...}: {
  extraPackages = [];
  plugins = with pkgs.vimPlugins; [
    gitsigns-nvim
  ];
}
