local M = {}
local lualine = require('lualine')

M.opts = {}
M.init = function()
  lualine.setup(M.opts)
end

return M
