{pkgs, ...}: {
  extraPackages = [];
  plugins = with pkgs.vimPlugins; [
    lualine-nvim
    nvim-web-devicons
  ];
}
