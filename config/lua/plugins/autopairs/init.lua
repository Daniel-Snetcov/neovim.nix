local M = {}
local npairs = require('nvim-autopairs')

M.opts = {
  fast_wrap = {},
}
M.init = function()
  npairs.setup(M.opts)
end

return M
