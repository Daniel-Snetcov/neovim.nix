{pkgs, ...}: {
  extraPackages = [];
  plugins = with pkgs.vimPlugins; [
    nvim-autopairs
  ];
}
