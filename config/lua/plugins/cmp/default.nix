{pkgs, ...}: {
  extraPackages = [];
  plugins = with pkgs.vimPlugins; [
    nvim-cmp
    cmp-buffer
    cmp-path
    luasnip
    cmp_luasnip
    lspkind-nvim

    nvim-autopairs
  ];
}
