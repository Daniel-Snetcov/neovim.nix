local M = {}
local lspconfig = require('lspconfig')

local function on_attach(client, buffer)
  local opts = { buffer = buffer, remap = false }
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
  vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
  vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
  vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
  vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)
  vim.keymap.set('n', 'gt', vim.lsp.buf.type_definition, opts)
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
  vim.keymap.set('n', '<leader>rn', vim.lsp.buf.rename, opts)
  vim.keymap.set({ 'n', 'v' }, '<leader>ca', vim.lsp.buf.code_action, opts)

  vim.keymap.set('n', '<leader>dl', '<cmd>Telescope diagnostics<CR>', opts)
  vim.keymap.set('n', '<leader>dn', vim.diagnostic.goto_next, opts)
  vim.keymap.set('n', '<leader>dp', vim.diagnostic.goto_prev, opts)
end

local function setup_language_servers()
  local language_servers = require('plugins.lspconfig.language-servers')
  local capabilities = require('cmp_nvim_lsp').default_capabilities()

  for server, opts in pairs(language_servers) do
    opts.on_attach = on_attach
    opts.capabilities = capabilities
    lspconfig[server].setup(opts)
  end
end

M.init = function()
  setup_language_servers()
end

return M
