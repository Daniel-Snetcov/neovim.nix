{pkgs, ...}: {
  extraPackages = with pkgs; [
    # Nix
    nil

    # Despair
    nodePackages."typescript-language-server"
    nodePackages."vscode-langservers-extracted"

    # lua
    lua-language-server

    # Docker
    nodePackages."dockerfile-language-server-nodejs"
    docker-compose-language-service

    # Misc
    nodePackages."bash-language-server"
    nodePackages."yaml-language-server"
    taplo
    terraform-ls
    tflint
    helm-ls
  ];
  plugins = with pkgs.vimPlugins; [
    nvim-lspconfig
    cmp-nvim-lsp
    vim-helm
  ];
}
