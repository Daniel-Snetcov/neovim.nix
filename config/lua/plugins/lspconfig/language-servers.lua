local M = {}
M.bashls = {}
M.dockerls = {}
M.jsonls = {}
M.taplo = {}
M.helm_ls = {}
M.lua_ls = {
  settings = {
    Lua = {
      diagnostics = {
        globals = { 'vim' },
      },
      runtime = {
        version = 'LuaJIT',
      },
      telemetry = {
        enable = false,
      },
    },
  },
}
M.nil_ls = {
  settings = {
    ['nil'] = {
      nix = {
        flake = {
          autoArchive = true,
          autoEvalInputs = true,
        },
      },
    },
  },
}
M.tsserver = {
  init_options = {
    preferences = {
      importModuleSpecifierPreference = 'relative',
    },
  },
}
M.terraformls = {}
M.tflint = {}

return M
