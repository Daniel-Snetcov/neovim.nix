{pkgs, ...}: {
  extraPackages = [];
  plugins = [
    (
      pkgs.vimUtils.buildVimPlugin
      {
        pname = "harpoon2";
        version = "2024-01-26";
        src = pkgs.fetchFromGitHub {
          owner = "ThePrimeagen";
          repo = "harpoon";
          rev = "a38be6e0dd4c6db66997deab71fc4453ace97f9c";
          sha256 = "1cmiw4sy5r4h8f2k1m91f2xykasnp66zdibx0l8vk94hw990sg26";
        };
        meta.homepage = "https://github.com/ThePrimeagen/harpoon/";
      }
    )
  ];
}
