local M = {}
local harpoon = require('harpoon')

local function setup_keymaps()
  vim.keymap.set('n', '<leader>he', function()
    harpoon.ui:toggle_quick_menu(harpoon:list())
  end)
  vim.keymap.set('n', '<leader>ha', function()
    harpoon:list():append()
  end)
  vim.keymap.set('n', '<C-A-u>', function()
    harpoon:list():select(1)
  end)
  vim.keymap.set('n', '<C-A-i>', function()
    harpoon:list():select(2)
  end)
  vim.keymap.set('n', '<C-A-o>', function()
    harpoon:list():select(3)
  end)
  vim.keymap.set('n', '<C-A-p>', function()
    harpoon:list():select(4)
  end)
  vim.keymap.set('n', '<C-A-P>', function()
    harpoon:list():prev()
  end)
  vim.keymap.set('n', '<C-A-N>', function()
    harpoon:list():next()
  end)
end

M.opts = {
  settings = {
    save_on_toggle = true,
    sync_on_ui_close = true,
  },
}
M.init = function()
  harpoon:setup()
  setup_keymaps()
end
return M
