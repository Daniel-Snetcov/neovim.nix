local M = {}
local oil = require('oil')

M.opts = {}
M.init = function()
  vim.keymap.set('n', '-', '<CMD>Oil<CR>', { desc = 'Open parent directory' })
  oil.setup(M.opts)
end

return M
