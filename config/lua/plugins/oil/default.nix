{pkgs, ...}: {
  extraPackages = with pkgs; [
  ];
  plugins = with pkgs.vimPlugins; [
    oil-nvim
  ];
}
