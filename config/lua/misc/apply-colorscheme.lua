function apply_colorscheme(colorscheme)
  colorscheme = colorscheme or 'catppuccin'
  vim.opt.background = 'dark'
  vim.cmd.colorscheme(colorscheme)
end

apply_colorscheme()
