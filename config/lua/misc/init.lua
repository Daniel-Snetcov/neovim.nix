local M = {}

M.init = function()
  require('misc.apply-colorscheme')
  require('misc.highlight-yank')
end

return M
