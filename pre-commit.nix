{config, ...}: {
  settings = {
    src = ./.;
    hooks = {
      treefmt = {
        enable = true;
        fail_fast = true;
        always_run = true;
        verbose = true;
      };
    };
    settings.treefmt.package = config.treefmt.build.wrapper;
  };
}
